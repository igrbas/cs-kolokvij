﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolokvij {
    sealed class Korisnik {

        public Korisnik(string korisnickoIme, string ime, int broj, bool imaMikrofon, bool imaKameru) {
            KorisnickoIme = korisnickoIme;
            Ime = ime;
            Broj = broj;
            ImaMikrofon = imaMikrofon;
            ImaKameru = imaKameru;
        }

        public override string ToString() {
            return string.Format("{0} / {1} / {2}", Broj, KorisnickoIme, Ime);
        }

        public string KorisnickoIme {
            get {
                return korisnickoIme;
            }
            set {
                korisnickoIme = value;
            }
        }

        public string Ime {
            get {
                return ime;
            }
            set {
                ime = value;
            }
        }

        public int Broj {
            get {
                return broj;
            }
            set {
                if (value > 100000) {
                    broj = value;
                }
                else {
                    throw new Exception("Broj mora biti veći od 100000");
                }
            }
        }

        public bool ImaMikrofon {
            get {
                return imaMikrofon;
            }
            set {
                imaMikrofon = value;
            }
        }

        public bool ImaKameru {
            get {
                return imaKameru;
            }
            set {
                imaKameru = value;
            }
        }

        private string korisnickoIme;
        private string ime;
        private int broj;
        private bool imaMikrofon;
        private bool imaKameru;
    }
}
