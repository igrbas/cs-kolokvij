﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolokvij {
    class VideoPoziv : Poziv, IPoziv{
        public VideoPoziv(DateTime pocetakPoziva, DateTime zavrsetakPoziva, string lozinka = "123456") : base(pocetakPoziva, zavrsetakPoziva, lozinka) { }

        public override void SpojiKorisnik(Korisnik korisnik) {
            if (korisnik.ImaMikrofon && korisnik.ImaKameru) {
                Korisnici.Add(korisnik);
            } else {
                throw new Exception("Korisnik nema potrebnu opremu");
            }
        }

        public override void PrikaziKorisnike() {
            Console.WriteLine("Korisnici video poziva:");
            foreach (Korisnik k in Korisnici) {
                Console.WriteLine(k.ToString());
            }
        }
    }
}
