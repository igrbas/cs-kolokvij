﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolokvij {
    class Program {
        static void Main(string[] args) {
            VideoPoziv videoPoziv = new VideoPoziv(DateTime.Now, DateTime.Now);
            try {
                videoPoziv.SpojiKorisnik(new Korisnik("tony123", "Tony", 1000000, true, true));
                videoPoziv.SpojiKorisnik(new Korisnik("mile123", "Milan", 1000001, true, true));
                videoPoziv.SpojiKorisnik(new Korisnik("anto123", "Antun", 1000002, true, true));
                videoPoziv.SpojiKorisnik(new Korisnik("mirko123", "Mirko", 1000003, false, true));
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            videoPoziv.PrikaziKorisnike();

            GlasovniPoziv glasovniPoziv = new GlasovniPoziv(DateTime.Now, DateTime.Now, "automoto123");
            try {
                glasovniPoziv.SpojiKorisnik(new Korisnik("ivan123", "Ivan", 2000000, true, false));
                glasovniPoziv.SpojiKorisnik(new Korisnik("cap123", "Steve", 2000001, true, false));
                glasovniPoziv.SpojiKorisnik(new Korisnik("marko123", "Marko", 2000002, true, true));
                glasovniPoziv.SpojiKorisnik(new Korisnik("nomic123", "Josip", 2000003, true, false));
                glasovniPoziv.SpojiKorisnik(new Korisnik("ivan123", "Ivan", 2000004, true, false));
                glasovniPoziv.SpojiKorisnik(new Korisnik("cap123", "Steve", 2000005, true, false));
                glasovniPoziv.SpojiKorisnik(new Korisnik("marko123", "Marko", 2000006, true, true));
                glasovniPoziv.SpojiKorisnik(new Korisnik("nomic123", "Josip", 2000007, true, false));
                glasovniPoziv.SpojiKorisnik(new Korisnik("ivan123", "Ivan", 2000008, true, false));
                glasovniPoziv.SpojiKorisnik(new Korisnik("cap123", "Steve", 2000009, true, false));
                glasovniPoziv.SpojiKorisnik(new Korisnik("marko123", "Marko", 2000010, true, true));
                glasovniPoziv.SpojiKorisnik(new Korisnik("nomic123", "Josip", 2000011, true, false));
                glasovniPoziv.SpojiKorisnik(new Korisnik("ivan123", "Ivan", 2000012, true, false));
                glasovniPoziv.SpojiKorisnik(new Korisnik("cap123", "Steve", 2000013, true, false));
                glasovniPoziv.SpojiKorisnik(new Korisnik("marko123", "Marko", 2000014, true, true));
                glasovniPoziv.SpojiKorisnik(new Korisnik("nomic123", "Josip", 2000015, true, false));
                glasovniPoziv.SpojiKorisnik(new Korisnik("nomic123", "Josip", 2000016, true, false));
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            glasovniPoziv.PrikaziKorisnike();
            try {
                glasovniPoziv.ZavrsetakPoziva = DateTime.Now;
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            try {
                videoPoziv.ZavrsetakPoziva = new DateTime(1999,12,12);
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            Console.ReadLine();
        }
    }
}
