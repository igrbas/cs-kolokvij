﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolokvij {
    class GlasovniPoziv : Poziv, IPoziv{
        public GlasovniPoziv(DateTime pocetakPoziva, DateTime zavrsetakPoziva, string lozinka) : base(pocetakPoziva,zavrsetakPoziva,lozinka) { }

        public override void SpojiKorisnik(Korisnik korisnik) {
            if(Korisnici.Count >= 16) {
                throw new Exception("U pozivu može biti maksimalno 16 korisnika");
            }
            if (korisnik.ImaMikrofon) {
                Korisnici.Add(korisnik);
            } else {
                throw new Exception("Korisnik nema potrebnu opremu");
            }
        }

        public override void PrikaziKorisnike() {
            Console.WriteLine("Korisnici glasovnog poziva: ");
            foreach(Korisnik k in Korisnici) {
                if (!k.ImaKameru) {
                    Console.WriteLine(k.ToString());
                }
            }
        }

    }
}
