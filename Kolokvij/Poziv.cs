﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolokvij {
    abstract class Poziv {

        public Poziv(DateTime pocetakPoziva, DateTime zavrsetakPoziva, string lozinka) {
            PocetakPoziva = pocetakPoziva;
            this.zavrsetakPoziva = zavrsetakPoziva;
            Lozinka = lozinka;
            Korisnici = new List<Korisnik>();
        }

        abstract public void SpojiKorisnik(Korisnik korisnik);

        public virtual void PrikaziKorisnike() {
            foreach(Korisnik k in Korisnici) {
                Console.WriteLine(k.ToString());
            }
        }

        public DateTime PocetakPoziva {
            get {
                return pocetakPoziva;
            }
            set {
                pocetakPoziva = value;
            }
        }

        public DateTime ZavrsetakPoziva {
            get {
                return zavrsetakPoziva;
            }
            set {
                if (value > zavrsetakPoziva) {
                    zavrsetakPoziva = value;
                } else {
                    throw new Exception("Nije moguće postaviti vrijeme završetka sastanka");
                }
            }
        }

        public string Lozinka {
            get {
                return lozinka;
            }
            set {
                lozinka = value;
            }
        }

        protected List<Korisnik> Korisnici { get; set; }
        private DateTime pocetakPoziva;
        private DateTime zavrsetakPoziva;
        private string lozinka;
    }
}
